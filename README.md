> Auteur : Jordan Protin | jordan.protin@yahoo.com

# Documentations

## Sommaire

- [DevOps](#devOps)
- [Utilitaires](#utilitaires)
- [Améliorer sa productivité](#améliorer-sa-productivité)
- [Développement](#développement)
  - [React, React-Native](#react-react-native)
  - [Ionic](#ionic)
  - [Symfony](#symfony)
  - [Généralités mobile](#généralités-mobile)
- [Démos](#démos)
- [Projets Raspberry Pi](#projets-raspberry-pi)

### DevOps

- [Rancher : utilisation générale](https://gitlab.com/jordanprotin/docs/-/wikis/Rancher-:-utilisation-g%C3%A9n%C3%A9rale)
- [Gitlab CI/CD : utilisation générale](https://gitlab.com/jordanprotin/docs/-/wikis/Gitlab-CI,CD-:-utilisation-g%C3%A9n%C3%A9rale)

### Utilitaires

- [Divers : tips & tricks](https://gitlab.com/jordanprotin/docs/-/wikis/Divers-:-tips-&-tricks)
- [SVN : guide des principales commandes](https://gitlab.com/jordanprotin/docs/-/wikis/SVN-:-guide-des-principales-commandes)
- [SSH : gestion des clés sur sa machine](https://gitlab.com/jordanprotin/docs/-/wikis/SSH-:-gestion-des-cl%C3%A9s-sur-sa-machine)
- [SSH : guide des principales commandes](https://gitlab.com/jordanprotin/docs/-/wikis/SSH-:-guide-des-principales-commandes)
- [SSH : script bash pour exécuter les commandes](https://gitlab.com/jordanprotin/public/docs/-/wikis/SSH-:-script-bash-pour-ex%C3%A9cuter-les-commandes-)
- [Docker : installation et principales commandes](https://gitlab.com/jordanprotin/docs/-/wikis/Docker-:-installation-et-principales-commandes)
- [Développement mobile : PWA vs Native app](https://gitlab.com/jordanprotin/docs/-/wikis/D%C3%A9veloppement-mobile-:-PWA-vs-Native-App-%3F)
- [Lighthouse by Google : un outil pour effectuer des audits web](https://gitlab.com/jordanprotin/docs/-/wikis/Lighthouse-:-un-outil-pour-effectuer-des-audits-web)
- [Markdown : liste des emojis](https://gitlab.com/jordanprotin/docs/-/wikis/Docs-:-liste-des-emojis-markdown)
- [Exemple de signature HTML pour son adresse mail](https://gitlab.com/jordanprotin/docs/-/wikis/Signature-email-%5Bexemple-by-TPZ%5D)

### Améliorer sa productivité

- [VSCode : extension SonarLint](https://gitlab.com/jordanprotin/docs/-/wikis/Visual-Studio-Code-:-extension-SonarLint)
- [Makefile](https://gitlab.com/jordanprotin/docs/-/wikis/Makefile-:-utilisation-simple)
- [Gitlab : gestion de projet](https://gitlab.com/jordanprotin/docs/-/wikis/Gitlab-:-gestion-de-projet)
- [Docs : construire un générateur de documentation markdown dans son projet](https://gitlab.com/jordanprotin/docs/-/wikis/Docs-:-construire-un-g%C3%A9n%C3%A9rateur-de-documentation-markdown-dans-son-projet)
- [GIT : les bonnes pratiques](https://gitlab.com/jordanprotin/docs/-/wikis/GIT-:-les-bonnes-pratiques)
- [SonarQube : l'intégrer dans son projet](https://gitlab.com/jordanprotin/docs/-/wikis/SonarQube-:-l'int%C3%A9grer-dans-son-projet)
- [Mes outils favoris et liens utiles](https://gitlab.com/jordanprotin/docs/-/wikis/Mes-outils-favoris-et-liens-utiles)

### Développement

#### React, React-Native

- [Librairie Easy-Peasy](https://gitlab.com/jordanprotin/docs/-/wikis/React-:-gestion-d'%C3%A9tat-avec-Easy-Peasy)
- [Formik feat Yup : validation des formulaires](https://gitlab.com/jordanprotin/docs/-/wikis/React-:-Validation-de-formulaires-avec-Formik-et-Yup)
- [CodePush (App Center) : l'intégrer dans un projet react-native](https://gitlab.com/jordanprotin/docs/-/wikis/CodePush-(App-center)-:-l'int%C3%A9grer-dans-un-projet-react-native)
- [Préparation au déploiement sur les stores](https://gitlab.com/jordanprotin/docs/-/wikis/React-Native-:-pr%C3%A9paration-au-d%C3%A9ploiement-sur-les-stores)

#### Ionic

- [Préparation au déploiement sur les stores](https://gitlab.com/jordanprotin/docs/-/wikis/Ionic-:-pr%C3%A9paration-au-d%C3%A9ploiement-sur-les-stores)

#### Symfony

- [Préparation d'un serveur o2switch](https://gitlab.com/jordanprotin/docs/-/wikis/Symfony-:-pr%C3%A9paration-d'un-serveur-o2switch)
- [Déploiement en production](https://gitlab.com/jordanprotin/docs/-/wikis/Symfony-:-d%C3%A9ploiement-en-production)

#### Généralités mobile

- [Publication d'une application sur les stores](https://gitlab.com/jordanprotin/docs/-/wikis/D%C3%A9veloppement-mobile-:-publication-d'une-application-sur-les-stores)
- [Génération d'un certificat iOS Push Notifications et update sur OneSignal](https://gitlab.com/jordanprotin/docs/-/wikis/G%C3%A9n%C3%A9ration-d'un-certificat-iOS-Push-Notifications-et-update-sur-OneSignal)

## Démos

- [HERE : API Geocoder](https://gitlab.com/jordanprotin/docs/-/wikis/HERE-:-API-Geocoder)
- [Docker : TUTO utilisation basique](https://gitlab.com/jordanprotin/docs/-/wikis/Docker-:-TUTO-utilisation-basique)
- [SonarQube](https://gitlab.com/jordanprotin/docs/-/wikis/SonarQube-:-CodeQuality)

## Projets Raspberry Pi

### Système de vidéosurveillance

- [Installation de MotionEye avec Raspberry Pi OS (Raspbian)](https://gitlab.com/jordanprotin/docs/-/wikis/TUTO-Raspberry-:-MotionEye-avec-Raspberry-Pi-OS-(Raspbian))
- [Installation de MotionEye avec Raspbian Buster](https://gitlab.com/jordanprotin/docs/-/wikis/TUTO-Raspberry-:-MotionEye-avec-Raspbian-Buster)
- [Configuration de MotionEye](https://gitlab.com/jordanprotin/docs/-/wikis/TUTO-Raspberry-:-vid%C3%A9osurveillance-avec-MotionEye)

### Serveur web

- [Installation d'un serveur web (Apache, PHP et MySQL)](https://gitlab.com/jordanprotin/docs/-/wikis/TUTO-Raspberry-:-Installation-d'un-serveur-web-(Apache,-PHP-et-MySQL))